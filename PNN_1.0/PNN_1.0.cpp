// PNN_1.0.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <fstream>
#include <iostream>
#include "pnn_cuda.h"
#include "serial_pnn.h"

#include <ctime>
#include <string>

#include "parallel_pnn.h"

using namespace std;




unsigned char train[NUMBER_OF_CLASSES][SIZE*CLASS_TRAIN_EXAMP];
unsigned char test[NUMBER_OF_CLASSES][SIZE*CLASS_TEST_EXAMP];

void read_files(unsigned char train[][SIZE*CLASS_TRAIN_EXAMP],unsigned char test[][SIZE*CLASS_TEST_EXAMP]);

int _tmain(int argc, _TCHAR* argv[])
{
	float probabilities[NUMBER_OF_CLASSES];


	read_files(train,test);

/*
	for(int k=0;k<28;k++)
	{

		for(int kk=0;kk<28;kk++)
		{
			if(train[9][kk+28*k]==0)
				cout<<0;
			else
				cout<<1;
			cout<<" ";
		}
		cout<<endl;
	}
*/



	clock_t begin = clock();

	serial_pnn(train,test);

	clock_t end = clock();

	float elapsed_secs = float(end - begin) / CLOCKS_PER_SEC;
	cout<<elapsed_secs<<endl<<endl;

	cout<<"cuda version"<<endl;

	parallel_pnn(train,test);


	
/*
	float d[]={12.2,13.3,14.4};
	float c[]={12.2,13.3,14.4};
	float b[]={12.2,13.3,14.4};
	cuda_call(d,b,c);*/

	

	//char x;
	//cin>>x;

	return 0;
}




void read_files(unsigned char train[][SIZE*CLASS_TRAIN_EXAMP],unsigned char test[][SIZE*CLASS_TEST_EXAMP])
{
	fstream file;
	const string names[]= {"data0","data1","data2","data3","data4","data5","data6","data7","data8","data9"};
	for(int k=0; k<NUMBER_OF_CLASSES;k++)
	{
		file.open( names[k], ios::in | ios::binary );
		file.read((char*)train[k], SIZE*CLASS_TRAIN_EXAMP);
		file.read((char*)test[k], SIZE*CLASS_TEST_EXAMP);
		file.close();
	}
}
