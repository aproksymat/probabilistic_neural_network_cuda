#include "params.h"
#include <cmath>

float gaussian(float distance)
{
	const float sigma=SIGMA;
	

	return exp( -distance / ( 2*pow(sigma,2) ) );
}

float compute_distance(unsigned char a, unsigned char b )
{
	float retVal = 0.0;
	float aa = static_cast<float>(a)/256.0;
	float bb = static_cast<float>(b)/256.0;
	//cout<<"in c_d"<<endl<<aa<<endl<<bb<<endl;

	retVal = bb-aa;
	
	return retVal;
}

bool is_max(float * tab, int class_id)
{
	float max=-0.1;
	int max_class;
	for(int k=0;k<NUMBER_OF_CLASSES;k++)
	{
		if(tab[k]>max)
		{
			max=tab[k];
			max_class=k;
		}
	}
	return max_class==class_id;
}