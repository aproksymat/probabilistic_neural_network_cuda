#include "stdafx.h"
#include <iostream>
#include <cmath>
#include "params.h"
#include "pnn_cuda.h"

using namespace std;

float gaussian(float distance);
float compute_distance(char a, char b );
bool is_max(float * tab, int class_id);

void parallel_pnn(unsigned char train[][SIZE*CLASS_TRAIN_EXAMP],unsigned char test[][SIZE*CLASS_TEST_EXAMP])
{
	
	float probabilities[NUMBER_OF_CLASSES];
	float partial_dist, class_probability,tmp;
	int wrongs=0;
	for(int tested_class=0;  tested_class<NUMBER_OF_CLASSES; tested_class++)
	{
		for(int tested=0; tested<CLASS_TEST_EXAMP; tested++)
		{

			pnn_class_parallel(train , &test[tested_class][tested*SIZE], probabilities);


		/*	for(int k=0; k<NUMBER_OF_CLASSES; k++)
			{
				cout<<probabilities[k]<<endl;
			}*/
/*		
			for(int class_id=0;class_id<NUMBER_OF_CLASSES;class_id++)
			{
				class_probability=0.0;
				for(int element=0;element<CLASS_TRAIN_EXAMP;element++)
				{

					partial_dist=0.0;

					for(int feature=0;feature<SIZE;feature++)
					{
						tmp = compute_distance(train[class_id][element*SIZE+feature],test[tested_class][tested*SIZE+feature]);
						partial_dist+= tmp*tmp;
					}

					class_probability+=gaussian(partial_dist);
				}
				probabilities[class_id]=class_probability;

			}
*/

			if(!is_max(probabilities,tested_class))
			{
				//cout<<"wrong!"<<endl;
				wrongs++;
			}
		}
		cout<<"tested class is "<<tested_class<<"\twrong: "<<wrongs<<endl;

		
	}
	cout<<"tested: "<<NUMBER_OF_CLASSES*CLASS_TEST_EXAMP<<endl;
	cout<<"wrong: "<<wrongs<<endl;
}
