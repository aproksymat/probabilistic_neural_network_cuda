#include "book.h"
#include <math.h>
#include "params.h"
#include "helpers.h"

__device__ float dev_gaussian(float distance)
{
	const float sigma=SIGMA;
	

	return exp( -distance / ( 2*pow(sigma,2) ) );
}

__device__ float dev_compute_distance(unsigned char a, unsigned char b )
{
	float retVal = 0.0;
	float aa = static_cast<float>(a)/256.0;
	float bb = static_cast<float>(b)/256.0;
	//cout<<"in c_d"<<endl<<aa<<endl<<bb<<endl;

	retVal = bb-aa;
	
	return retVal;
}


__global__ void w1(float * product1, float * pic, float * wage1)
{
	int input=threadIdx.x;
	float inpe=input*2.71;
}

__global__ void dev_pnn_class_parallel(unsigned char *dev_train, unsigned char *dev_test_element, float *dev_probabilities)
{
	int class_id=blockIdx.x;
	float tmp;

	float class_probability=0.0;

	for(int element=0;element<CLASS_TRAIN_EXAMP;element++)
	{

		float partial_dist=0.0;

		for(int feature=0;feature<SIZE;feature++)
		{
			tmp = dev_compute_distance(dev_train[class_id*SIZE*CLASS_TRAIN_EXAMP  +element*SIZE+feature],dev_test_element[feature]);
			partial_dist+= tmp*tmp;
		}

		class_probability+=dev_gaussian(partial_dist);
	}
	dev_probabilities[class_id]=class_probability;



	//dev_probabilities[class_id] = 0.01*class_id;

}
void cuda_call(float * a, float * b, float * c)
{
	w1<<<1,1>>>(a, b, c);
}


//void train_set_malloc(void)
void pnn_class_parallel(unsigned char train[][SIZE*CLASS_TRAIN_EXAMP],unsigned char test_element[], float probabilities[])
{
	unsigned char *dev_train;
	unsigned char *dev_test_element;
	float *dev_probabilities;


	cudaMalloc( (void**)&dev_train, NUMBER_OF_CLASSES*SIZE*CLASS_TRAIN_EXAMP*sizeof(unsigned char) );
	cudaMemcpy( dev_train, train, NUMBER_OF_CLASSES*SIZE*CLASS_TRAIN_EXAMP*sizeof(unsigned char), cudaMemcpyHostToDevice );




	cudaMalloc( (void**)&dev_test_element, SIZE*sizeof(unsigned char) );
	cudaMalloc( (void**)&dev_probabilities, NUMBER_OF_CLASSES*sizeof(float) );

	cudaMemcpy( dev_train, train, NUMBER_OF_CLASSES*SIZE*CLASS_TRAIN_EXAMP*sizeof(unsigned char), cudaMemcpyHostToDevice );
	cudaMemcpy( dev_test_element, test_element, SIZE*sizeof(unsigned char), cudaMemcpyHostToDevice );

	dev_pnn_class_parallel<<<NUMBER_OF_CLASSES,1>>>(dev_train, dev_test_element, dev_probabilities);

	cudaMemcpy( probabilities, dev_probabilities,  NUMBER_OF_CLASSES*sizeof(float), cudaMemcpyDeviceToHost );



	/*
	for(int class_id=0;class_id<NUMBER_OF_CLASSES;class_id++)
	{
		class_probability=0.0;
		for(int element=0;element<CLASS_TRAIN_EXAMP;element++)
		{

			partial_dist=0.0;

			for(int feature=0;feature<SIZE;feature++)
			{
				tmp = compute_distance(train[class_id][element*SIZE+feature],test_element[feature]);
				partial_dist+= tmp*tmp;
			}

			class_probability+=gaussian(partial_dist);
		}
		probabilities[class_id]=class_probability;

	}
	*/
	cudaFree( dev_train );
	cudaFree( dev_test_element );
	cudaFree( dev_probabilities );

	cudaDeviceReset();
}