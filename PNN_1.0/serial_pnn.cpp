

#include "stdafx.h"
#include <iostream>
#include <cmath>
#include "params.h"
#include "helpers.h"
using namespace std;


void serial_pnn(unsigned char train[][SIZE*CLASS_TRAIN_EXAMP],unsigned char test[][SIZE*CLASS_TEST_EXAMP])
{
	float probabilities[NUMBER_OF_CLASSES];
	float partial_dist, class_probability,tmp;
	int wrongs=0;
	for(int tested_class=0;  tested_class<NUMBER_OF_CLASSES; tested_class++)
	{
		for(int tested=0; tested<CLASS_TEST_EXAMP; tested++)
		{
			//cout<<"tested is: "<<endl;
	/*			for(int k=0;k<28;k++)
				{

					for(int kk=0;kk<28;kk++)
					{
						if(test[0][tested*SIZE+kk+28*k]==0)
							cout<<0;
						else if(test[0][tested*SIZE+kk+28*k]==255)
							cout<<1;
						else
							cout<<"+";
						cout<<" ";
					}
					cout<<endl;
				}
	*/

			//cout<<endl;
			for(int class_id=0;class_id<NUMBER_OF_CLASSES;class_id++)
			{
				class_probability=0.0;
				for(int element=0;element<CLASS_TRAIN_EXAMP;element++)
				{

							/*
							//if(class_id==4)
							//{
							
								cout<<"element is: "<<endl;
								for(int k=0;k<28;k++)
								{

									for(int kk=0;kk<28;kk++)
									{
										if(train[class_id][element*SIZE+kk+28*k]==0)
											cout<<0;
										else if(train[class_id][element*SIZE+kk+28*k]>1)
											cout<<1;
										else
											cout<<"+";
										cout<<" ";
									}
									cout<<endl;
								}
							
						//	}
						*/
					partial_dist=0.0;
					for(int feature=0;feature<SIZE;feature++)
					{
						tmp = compute_distance(train[class_id][element*SIZE+feature],test[tested_class][tested*SIZE+feature]);
						//if(tmp!=0.0);
							//cout<<"not zero distance";
						partial_dist+= tmp*tmp;
					}
					//cout<<"dist is: "<<partial_dist<<endl;
					class_probability+=gaussian(partial_dist);
				}
				probabilities[class_id]=class_probability;
				//cout<<"prob of "<<class_id<<" is: "<<class_probability<<endl;
			}
			if(!is_max(probabilities,tested_class))
			{
				//cout<<"wrong!"<<endl;
				wrongs++;
			}
		}
		cout<<"tested class is "<<tested_class<<"\twrong: "<<wrongs<<endl;

		
	}
	cout<<"tested: "<<NUMBER_OF_CLASSES*CLASS_TEST_EXAMP<<endl;
	cout<<"wrong: "<<wrongs<<endl;
}

